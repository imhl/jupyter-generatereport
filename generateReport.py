import csv
import json
import sys

studentSubFileFlag = False
studentComFileFlag = False

# Read studentSubmission file
try:
    with open(sys.argv[1]) as studentsubmissionfile:
        data = json.load(studentsubmissionfile)
        studentSubFileFlag = True
    studentsubmissionfile.close()
except IOError:
    print("Student Submission File not Found.")
    studentSubFileFlag = False
except BaseException:
    print("==== Usage ==== \n\npython generateReport.py <studentSubmission.ipynb> <comments.csv>")
    studentSubFileFlag = False
except:
    raise


# Print newlines and insert code comments in code cell along with the marker name
def insertCodeCommentsByLecturer(lectName, linetoInsert):
    insertcomments.insert(linetoInsert, "\n\n")
    insertcomments.insert(linetoInsert + 1, "# Comments by " + lectName + ":\n\n")
    insertcomments.insert(linetoInsert + 2, "# " + line['Code comment'])
    insertcomments.insert(linetoInsert + 3, "\n")
    insertcomments.insert(linetoInsert + 4, "# ====  End of Comment  ====")
    insertcomments.insert(linetoInsert + 5, "\n\n")


# Print newlines and insert code comments in markdown cell with the marker name
def appendMarkdownCommentsByLecturer(lectName):
    insertcomments.append("\n\n")
    insertcomments.append("Comments by " + lectName + ":\n\n")
    insertcomments.append(line['Code comment'])
    insertcomments.append("\n")
    insertcomments.append("=== End of Comment ===")
    insertcomments.append("\n\n")


# Read CSV file and insert comments
try:
    commentsfile = open(sys.argv[2], 'r')
    studentComFileFlag = True
    if studentSubFileFlag:
        reader = csv.DictReader(commentsfile)
        #print table header
        print('{:<10s}{:>4s}{:>12s}'.format("Cell_Type","Row Number", "Line Number"))
        print("-" *35)
        for line in reader:
            if line['Cell'] != '':
                cellnum = int(line['Cell']) - 1
                linetoInsert = int(line['Line number'])
                insertcomments = data['cells'][cellnum]['source']
                print('{:<10s}{:>4s}{:>12s}'.format(data['cells'][cellnum]['cell_type'], line['Cell'], line['Line number']))
                if data['cells'][cellnum]['cell_type'] == "code":
                    insertCodeCommentsByLecturer(line['Reviewer'], linetoInsert)
                else:
                    appendMarkdownCommentsByLecturer(line['Reviewer'])

except IOError:
    print("Comments file not found.")
    studentComFileFlag = False
except BaseException:
    studentComFileFlag = False
except:
    raise

# Write File
try:
    if studentSubFileFlag and studentComFileFlag:
        filename = sys.argv[1].split(".")[0] + "_withComments.ipynb"
        with open(filename, 'w') as outfile:
            json.dump(data, outfile, indent=4)
        print("File written as " + filename)
except:
    raise
